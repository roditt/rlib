package net.site40.rodit.rlib.sockets.tcp.client;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import net.site40.rodit.rlib.sockets.tcp.DisconnectReason;
import net.site40.rodit.rlib.util.Data;

/**
 * This class wraps around a Java {@link #Socket}, allowing simplified communication between sockets.
 */
public class Client {

	/**
	 * The default time that the client's thread sleeps between consecutive read calls to the socket input stream.
	 */
	public static final long DEFAULT_READ_SLEEP_TIME = 25L;
	/**
	 * The default size of the buffer used to read from the socket input stream.
	 */
	public static final int DEFAULT_READ_BUFFER_SIZE = 4096;

	private Socket socket;

	private long sleepTime;

	private Thread thread;
	private InputStream in;
	private OutputStream out;

	private ByteArrayOutputStream currentRead;
	private byte[] readBuffer;

	private int waitBufferExpectedLength = 0;
	private byte[] waitBuffer = new byte[0];

	private List<IClientListener> listeners;
	
	private Runnable runOnUpdateThread;

	/**
	 * Creates a new client and underlying socket with the default configuration.
	 * @see #DEFAULT_READ_SLEEP_TIME
	 * @see #DEFAULT_READ_BUFFER_SIZE
	 */
	public Client(){
		this(DEFAULT_READ_SLEEP_TIME);
	}

	/**
	 * Creates a new client and underlying socket with the specified sleep time.
	 * @param sleepTime The time to sleep the client's thread between consecutive read calls to the socket input stream.
	 * @see #DEFAULT_READ_BUFFER_SIZE
	 */
	public Client(long sleepTime){
		this(sleepTime, DEFAULT_READ_BUFFER_SIZE);
	}

	/**
	 * Creates a new client and underlying socket with the specified sleep time and read buffer size.
	 * @param sleepTime The time to sleep the client's thread between consecutive read calls to the socket input stream.
	 * @param readBufferSize The size of the buffer used to read the socket input stream.
	 */
	public Client(long sleepTime, int readBufferSize){
		this.socket = new Socket();
		this.sleepTime = sleepTime;
		this.readBuffer = new byte[readBufferSize];

		this.listeners = Collections.synchronizedList(new ArrayList<IClientListener>());
	}
	
	/**
	 * Get the client's working thread.
	 * @return The client's thread that is used to read data from the socket and detect disconnects and exceptions.
	 */
	public Thread getThread(){
		return thread;
	}

	/**
	 * Add a listener to the client. If the client's current list of listeners contains the provided listener, the provided listener is not added again.
	 * @param listener The listener to be added to the client's current list of listeners.
	 * @see #IClientListener
	 */
	public void addListener(IClientListener listener){
		if(!listeners.contains(listener))
			listeners.add(listener);
	}

	/**
	 * Remove a listener from the client.
	 * @param listener The listener to be removed from the client's current list of listeners.
	 * @see #IClientListener
	 */
	public void removeListener(IClientListener listener){
		listeners.remove(listener);
	}

	/**
	 * Checks whether the client is connected to a host or not.
	 * <br>
	 * <br>
	 * <b>Note:</b> This method relies on the client's socket being closed by a failed to or read from the socket.
	 * @return Whether the client is connected to a host or not.
	 */
	public boolean isConnected(){
		return socket != null && socket.isConnected() && !socket.isClosed();
	}
	
	/**
	 * Sets a runnable who's {@link Runnable#run()} will be run on every loop of the client's thread.
	 * This can be set to null to stop any runnable from being run in the client's thread's loop.
	 * @param runnable The runnable who's {@link Runnable#run()} method will be run on the client's thread.
	 */
	public void runOnUpdateThread(Runnable runnable){
		this.runOnUpdateThread = runnable;
	}

	/**
	 * Connects the client's socket to the specified host and port.
	 * @param host The host for the client to connect to.
	 * @param port The port for the client to connect to.
	 * @throws IOException Passes any IOException thrown by the {@link Socket#connect(java.net.SocketAddress)} method.
	 */
	public void connect(String host, int port)throws IOException{
		if(isConnected())
			throw new IllegalStateException("Cannot connect to server when socket is already connected.");
		socket.connect(new InetSocketAddress(host, port));

		in = socket.getInputStream();
		out = socket.getOutputStream();

		thread = new Thread(){
			@Override
			public void run(){
				onConnected();
				boolean errorClosed = false;
				while(isConnected()){
					try{
						currentRead = new ByteArrayOutputStream();
						int read = 0;
						while(in.available() > 0 && (read = in.read(readBuffer)) > 0)
							currentRead.write(readBuffer, 0, read);

						if(read == -1 || in.available() == -1)
							throw new IOException("Server socket closed.");

						if(currentRead.size() > 0){
							byte[] cBuffer = currentRead.toByteArray();
							currentRead.close();
							currentRead = null;
							read = 0;
							while(read < cBuffer.length){
								if(waitBuffer != null && waitBufferExpectedLength > 0 && waitBufferExpectedLength - waitBuffer.length > 0){
									int remainWaitBufferLength = waitBufferExpectedLength - waitBuffer.length;
									int toWrite = remainWaitBufferLength > cBuffer.length ? cBuffer.length : remainWaitBufferLength;
									byte[] waitBufferAdd = new byte[toWrite];
									System.arraycopy(cBuffer, 0, waitBufferAdd, 0, toWrite);
									waitBuffer = Data.concat(waitBuffer, waitBufferAdd);
									if(waitBuffer.length == waitBufferExpectedLength){
										onDataReceived(waitBuffer);
										waitBuffer = null;
										break;
									}
									if(cBuffer.length - toWrite > 0){
										byte[] ncbuffer = new byte[cBuffer.length - toWrite];
										System.arraycopy(cBuffer, toWrite, ncbuffer, 0, cBuffer.length - toWrite);
										cBuffer = ncbuffer;
									}else
										break;
								}

								byte[] pLengthData = new byte[4];
								System.arraycopy(cBuffer, read, pLengthData, 0, 4);
								int pLength = Data.convertInt(pLengthData);
								pLengthData = null;
								read += 4;

								if(pLength > cBuffer.length){
									waitBuffer = cBuffer;
									waitBufferExpectedLength = pLength;
									break;
								}

								byte[] packet = new byte[pLength];
								System.arraycopy(cBuffer, read, packet, 0, pLength);
								onDataReceived(packet);
								packet = null;
								read += pLength;
							}
							cBuffer = null;
						}
						
						if(runOnUpdateThread != null)
							runOnUpdateThread.run();
					}catch(IOException e){
						if(isConnected()){
							try{
								disconnect();
							}catch(IOException e0){}
							onDisconnected(DisconnectReason.CONNECTION_CLOSED);
							errorClosed = true;
						}else
							onClientException(e);
					}
					
					try{
						if(sleepTime > 0L)
							Thread.sleep(sleepTime);
					}catch(InterruptedException e){}
				}
				if(!errorClosed)
					onDisconnected(DisconnectReason.CLIENT_DISCONNECTED);
			}
		};
		thread.start();
	}
	
	/**
	 * Get the local socket being used by the client.
	 * @return The local socket currently being used by this client.
	 */
	public Socket getSocket(){
		return socket;
	}
	
	protected void onConnected(){
		for(IClientListener listener : listeners)
			listener.onConnected(Client.this);
	}

	protected void onDisconnected(DisconnectReason reason){
		for(IClientListener listener : listeners)
			listener.onDisconnected(Client.this, reason);
	}

	protected void onDataReceived(byte[] data){
		for(IClientListener listener : listeners)
			listener.onDataReceived(Client.this, data);
	}

	protected void onClientException(Exception e){
		for(IClientListener listener : listeners)
			listener.onClientException(Client.this, e);
	}

	/**
	 * Closes the underlying socket if it is open.
	 * @throws IOException Passes any IOException thrown by the {@link Socket#close()} method.
	 */
	public void disconnect()throws IOException{
		if(!isConnected())
			throw new IllegalStateException("Cannot disconnect from server when socket is not connected or open.");
		socket.close();
		socket = new Socket();
	}

	/**
	 * Writes the specified data to the socket output stream.
	 * @param data The buffer to be written to the socket output stream.
	 * @throws IOException Passes any IOException thrown by the {@link OutputStream#write(byte[])} method.
	 */
	public void write(byte[] data)throws IOException{
		try{
			byte[] full = Data.concat(Data.convertBytes(data.length), data);
			writeRaw(full);
			full = null;
		}catch(IOException e){
			if(e.getMessage().toLowerCase().contains("broken pipe") || e.getMessage().toLowerCase().contains("connection reset"))
				onDisconnected(DisconnectReason.CONNECTION_CLOSED);
			else
				throw e;
		}
	}
	
	protected void writeRaw(byte[] data)throws IOException{
		out.write(data);
		out.flush();
	}
}
