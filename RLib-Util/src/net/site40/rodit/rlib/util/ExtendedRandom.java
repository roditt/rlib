package net.site40.rodit.rlib.util;

import java.security.SecureRandom;

@SuppressWarnings("serial")
public class ExtendedRandom extends SecureRandom{

	public static final String CHARS_ALPHA_LOWER = "abcdefghijklmnopqrstuvwxyz";
	public static final String CHARS_ALPHA_UPPER = CHARS_ALPHA_LOWER.toUpperCase();
	public static final String CHARS_ALPHA_ALL = CHARS_ALPHA_LOWER + CHARS_ALPHA_UPPER;
	public static final String CHARS_NUMERIC = "1234567890";
	public static final String CHARS_ALPHA_NUMERIC = CHARS_ALPHA_ALL + CHARS_NUMERIC;
	public static final String CHARS_SYMBOL = "!\"�$%^&*()-=_+[]{};'#:@~,./<>?\\|`� 	";
	public static final String CHARS_ALL = CHARS_ALPHA_NUMERIC + CHARS_SYMBOL;
	
	public String nextString(int length){
		return nextString(length, CHARS_ALPHA_NUMERIC);
	}
	
	public String nextString(int length, String include){
		String rand = "";
		for(int i = 0; i < length; i++)
			rand += include.charAt(nextInt(include.length() - 1));
		return rand;
	}
	
	public int nextInt(int min, int max){
		if(max < min)
			throw new IllegalArgumentException("max must be greater or equal to min.");
		return min + nextInt(max - min);
	}
	
	public byte[] nextBytes(int length){
		byte[] buffer = new byte[length];
		nextBytes(buffer);
		return buffer;
	}
	
	public boolean should(float thresh){
		return nextFloat() > (1f - thresh);
	}
}
