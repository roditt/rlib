package net.site40.rodit.rlib.crypto;

import java.security.MessageDigest;

public class CryptoUtil {

	public static final byte DEFAULT_INC = 5;
	public static final byte DEFAULT_DEC = 5;

	public static void incAlgo(byte[] data, byte inc){
		for(int i = 0; i < data.length; i++)
			data[i] = (byte)(data[i] + inc);
	}

	public static void decAlgo(byte[] data, int dec){
		for(int i = 0; i < data.length; i++)
			data[i] = (byte)(data[i] - dec);
	}

	public static byte[] sha256(byte[] data){
		try{
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(data);
			return md.digest();
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public static byte[] md5(byte[] data){
		try{
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(data);
			return md.digest();
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public static byte[] read(byte[] src, int offset, int length){
		byte[] read = new byte[length];
		System.arraycopy(src, offset, read, 0, length);
		return read;
	}
	
	public static byte[] xor(byte[] b0, byte[] b1){
		if(b0.length != b1.length)
			return b0;
		byte[] xored = new byte[b0.length];
		for(int i = 0; i < b0.length; i++)
			xored[i] = (byte)(b0[i] ^ b1[i]);
		return xored;
	}
	
	public static byte[] shift(byte[] b0, byte[] b1){
		if(b0.length != b1.length)
			return b0;
		byte[] shifted = new byte[b0.length];
		for(int i = 0; i < b0.length; i++)
			shifted[i] = (byte)(b0[i] >>> b1[i]);
		return shifted;
	}
	
	public static byte[][] genKeys(byte[] data){
		if(data.length == 0)
			return new byte[0][0];
		byte[][] keys = new byte[8][16];
		byte[] sha = sha256(data);
		
		keys[0] = read(sha, 0, 16);
		keys[1] = read(sha, 16, 16);
		keys[2] = shift(keys[0], keys[1]);
		keys[3] = xor(keys[0], keys[2]);
		
		byte[] md5 = md5(data);
		keys[4] = md5;
		keys[5] = shift(keys[4], keys[1]);
		keys[6] = xor(keys[5], keys[4]);
		keys[7] = shift(xor(keys[0], keys[3]), xor(keys[0], shift(keys[4], keys[2])));
		
		return keys;
	}
}
