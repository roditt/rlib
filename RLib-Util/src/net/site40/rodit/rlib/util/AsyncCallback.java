package net.site40.rodit.rlib.util;

public interface AsyncCallback {

	public void callback(Object[] args);
}
