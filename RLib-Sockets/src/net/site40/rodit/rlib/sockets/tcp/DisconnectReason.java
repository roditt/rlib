package net.site40.rodit.rlib.sockets.tcp;

/**
 * The reason for a socket disconnection.
 * 
 */
public enum DisconnectReason {

	
	/**
	 * The remote socket was closed.
	 */
	CLIENT_DISCONNECTED,
	/**
	 * The connection was closed.
	 */
	CONNECTION_CLOSED,
	/**
	 * The connection thread was ended due to an unknown exception.
	 */
	UNKNOWN_EXCEPTION;
}
