package net.site40.rodit.rlib.util.io;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;

public class SeekableByteArrayOutputStream extends ByteArrayOutputStream{

	public SeekableByteArrayOutputStream(){
		super();
	}

	public void setPosition(int position){
		this.count = position;
		ensureCapacity(position);
	}

	public void seek(int seek){
		setPosition(count + seek);
	}

	private void ensureCapacity(int paramInt){
		if(paramInt - this.buf.length > 0)
			grow(paramInt);
	}

	private void grow(int paramInt){
		int i = this.buf.length;
		int j = i << 1;
		if (j - paramInt < 0) {
			j = paramInt;
		}
		if (j - 2147483639 > 0) {
			j = hugeCapacity(paramInt);
		}
		this.buf = Arrays.copyOf(this.buf, j);
	}

	private static int hugeCapacity(int paramInt){
		if(paramInt < 0)
			throw new OutOfMemoryError();
		return paramInt > 2147483639 ? Integer.MAX_VALUE : 2147483639;
	}
}
