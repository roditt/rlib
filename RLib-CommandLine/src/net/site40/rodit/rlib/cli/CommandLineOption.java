package net.site40.rodit.rlib.cli;

import java.io.PrintStream;
import java.util.ArrayList;

public class CommandLineOption {

	private ArrayList<String> tokens;
	private String usage;
	private boolean hasValue;
	private boolean required;
	
	public CommandLineOption(String token, String usage, boolean hasValue, boolean required, String... aliases){
		this.tokens = new ArrayList<String>();
		tokens.add(token.trim());
		if(aliases != null && aliases.length > 0)
			for(int i = 0; i < aliases.length; i++)
				tokens.add(aliases[i].trim());
		this.usage = usage;
		this.hasValue = hasValue;
		this.required = required;
	}
	
	public String getToken(){
		if(tokens.size() == 0)
			return "null";
		return tokens.get(0);
	}
	
	public ArrayList<String> getTokens(){
		return tokens;
	}
	
	public String getUsage(){
		return usage;
	}
	
	public void printUsage(){
		printUsage(System.out);
	}
	
	public void printUsage(PrintStream stream){
		stream.println("-" + getToken() + " " + usage);
	}
	
	public boolean hasValue(){
		return hasValue;
	}
	
	public boolean isRequired(){
		return required;
	}
}
