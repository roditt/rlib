package net.site40.rodit.rlib.util;

import java.math.RoundingMode;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class Data {

	public static byte[] convertBytes(short s){
		ByteBuffer buff = ByteBuffer.allocate(2);
		buff.putShort(s);
		buff.flip();
		byte[] data = new byte[2];
		buff.get(data);
		buff.clear();
		return data;
	}

	public static byte[] convertBytes(int i){
		ByteBuffer buff = ByteBuffer.allocate(4);
		buff.putInt(i);
		buff.flip();
		byte[] data = new byte[4];
		buff.get(data);
		buff.clear();
		return data;
	}

	public static byte[] convertBytes(long l){
		ByteBuffer buff = ByteBuffer.allocate(8);
		buff.putLong(l);
		buff.flip();
		byte[] data = new byte[8];
		buff.get(data);
		buff.clear();
		return data;
	}

	public static byte[] convertBytes(float f){
		ByteBuffer buff = ByteBuffer.allocate(4);
		buff.putFloat(f);
		buff.flip();
		byte[] data = new byte[4];
		buff.get(data);
		buff.clear();
		return data;
	}

	public static byte[] convertBytes(double d){
		ByteBuffer buff = ByteBuffer.allocate(8);
		buff.putDouble(d);
		buff.flip();
		byte[] data = new byte[8];
		buff.get(data);
		buff.clear();
		return data;
	}

	public static byte[] convertBytes(boolean b){
		return new byte[] { (byte)(b ? 1 : 0) };
	}

	public static short convertShort(byte[] data){
		if(data.length < 2)
			return 0;
		ByteBuffer buff = ByteBuffer.allocate(2);
		buff.put(data, 0, 2);
		buff.flip();
		short s = buff.getShort();
		buff.clear();
		return s;
	}

	public static int convertInt(byte[] data){
		if(data.length < 4)
			return 0;
		ByteBuffer buff = ByteBuffer.allocate(4);
		buff.put(data, 0, 4);
		buff.flip();
		int i = buff.getInt();
		buff.clear();
		return i;
	}

	public static long convertLong(byte[] data){
		if(data.length < 8)
			return 0;
		ByteBuffer buff = ByteBuffer.allocate(8);
		buff.put(data, 0, 8);
		buff.flip();
		long l = buff.getLong();
		buff.clear();
		return l;
	}

	public static float convertFloat(byte[] data){
		if(data.length < 4)
			return 0;
		ByteBuffer buff = ByteBuffer.allocate(4);
		buff.put(data, 0, 4);
		buff.flip();
		float f = buff.getFloat();
		buff.clear();
		return f;
	}

	public static double convertDouble(byte[] data){
		if(data.length < 8)
			return 0;
		ByteBuffer buff = ByteBuffer.allocate(8);
		buff.put(data, 0, 8);
		buff.flip();
		double d = buff.getDouble();
		buff.clear();
		return d;
	}

	public static boolean convertBool(byte[] data){
		return data[0] == 1;
	}
	
	public static String concat(String... strs){
		String full = "";
		for(int i = 0; i < strs.length; i++)
			full += strs[i] + (i == strs.length - 1 ? "" : " ");
		return full;
	}

	public static byte[] concat(byte[]... arrays){
		int length = 0;
		for(int i = 0; i < arrays.length; i++)
			length += arrays[i].length;

		byte[] full = new byte[length];
		int totalWritten = 0;
		for(int i = 0; i < arrays.length; i++){
			System.arraycopy(arrays[i], 0, full, totalWritten, arrays[i].length);
			totalWritten += arrays[i].length;
		}
		return full;
	}

	public static int convertInt(String data){
		return convertInt(data, 0);
	}

	public static int convertInt(String data, int ret){
		try{
			return Integer.valueOf(data);
		}catch(NumberFormatException e){}
		return ret;
	}

	public static long convertLong(String data){
		return convertLong(data, 0l);
	}

	public static long convertLong(String data, long ret){
		try{
			return Long.valueOf(data);
		}catch(NumberFormatException e){}
		return ret;
	}

	public static float convertFloat(String data){
		return convertFloat(data, 0f);
	}

	public static float convertFloat(String data, float ret){
		try{
			return Float.valueOf(data);
		}catch(NumberFormatException e){}
		return ret;
	}

	public static double convertDouble(String data){
		return convertDouble(data, 0d);
	}

	public static double convertDouble(String data, double ret){
		try{
			return Double.valueOf(data);
		}catch(NumberFormatException e){}
		return ret;
	}

	public static boolean convertBool(String data){
		return Boolean.valueOf(data);
	}

	public static String convertString(Object data){
		return String.valueOf(data);
	}

	public static boolean startsWith(byte[] start, byte[] full){
		if(start.length > full.length)
			return false;
		for(int i = 0; i < start.length; i++)
			if(start[i] != full[i])
				return false;
		return true;
	}

	public static byte[] read(byte[] full, int offset, int length){
		if(offset + length > full.length)
			return new byte[0];
		byte[] read = new byte[length];
		System.arraycopy(full, offset, read, 0, length);
		return read;
	}

	public static String appendArrays(String seperator, String[]... arrays){
		String full = "";
		for(int i = 0; i < arrays.length; i++)
			full += arrays[i] + (i < arrays.length - 1 ? seperator : "");
		return full;
	}

	public static boolean arrayContains(Object[] haystack, Object needle){
		for(int i = 0; i < haystack.length; i++)
			if(haystack[i] == needle)
				return true;
		return false;
	}

	public static boolean isValidURL(String url){
		URL u = null;

		try{
			u = new URL(url);  
		}catch(MalformedURLException e){  
			return false;  
		}

		try{  
			u.toURI();  
		}catch(URISyntaxException e){  
			return false;  
		}  

		return true;  
	}

	public static String humanReadableTimestamp(long timestamp){
		return new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date(timestamp));
	}

	public static final long SIZE_KB = 1024;
	public static final long SIZE_MB = SIZE_KB * 1024;
	public static final long SIZE_GB = SIZE_MB * 1024;

	public static String humanReadableFileSize(long size){
		if(size >= SIZE_GB)
			return round3dp(((double)size / (double)SIZE_GB)) + "GiB";
		else if(size >= SIZE_MB)
			return round3dp(((double)size / (double)SIZE_MB)) + "MiB";
		else if(size >= SIZE_KB)
			return round3dp(((double)size / (double)SIZE_KB)) + "KiB";
		else
			return size + "B";
	}

	public static String round3dp(double value){
		DecimalFormat df = new DecimalFormat("#.###");
		df.setRoundingMode(RoundingMode.CEILING);
		return df.format(value);
	}

	public static boolean isMatch(byte[] pattern, byte[] input, int pos){
		for(int i = 0; i< pattern.length; i++){
			if(pattern[i] != input[pos + i]){
				return false;
			}
		}
		return true;
	}

	public static List<byte[]> split(byte[] pattern, byte[] input){
		List<byte[]> l = new LinkedList<byte[]>();
		int blockStart = 0;
		for(int i = 0; i<input.length; i++){
			if(isMatch(pattern,input,i)){
				l.add(Arrays.copyOfRange(input, blockStart, i));
				blockStart = i+pattern.length;
				i = blockStart;
			}
		}
		l.add(Arrays.copyOfRange(input, blockStart, input.length));
		return l;
	}
}
