package net.site40.rodit.rlib.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class ByteArrayWriter {
	
	private static class SeekableBAOS extends ByteArrayOutputStream{
		
		public SeekableBAOS(){
			super();
		}
		
		public int position(){
			return count;
		}
		
		public void position(int position){
			this.count = position;
		}
	}

	private SeekableBAOS buffer;
	
	public ByteArrayWriter(){
		this.buffer = new SeekableBAOS();
	}
	
	public int position(){
		return buffer.position();
	}
	
	public void position(int position){
		buffer.position(position);
	}
	
	public byte[] getBuffer(){
		return buffer.toByteArray();
	}
	
	public void write(byte[] data)throws IOException{
		buffer.write(data);
	}
	
	public void write(byte data)throws IOException{
		write(new byte[] { data });
	}
	
	public void write(int data)throws IOException{
		write(Data.convertBytes(data));
	}
	
	public void write(long data)throws IOException{
		write(Data.convertBytes(data));
	}
	
	public void write(float data)throws IOException{
		write(Data.convertBytes(data));
	}
	
	public void write(double data)throws IOException{
		write(Data.convertBytes(data));
	}
	
	public void write(boolean data)throws IOException{
		write(Data.convertBytes(data));
	}
	
	public void write(String data)throws IOException{
		write(Data.convertBytes(data.length()));
		writeStringNoHead(data);
	}
	
	public void writeStringNoHead(String data)throws IOException{
		write(data.getBytes());
	}
	
	public void dispose()throws IOException{
		buffer.close();
		buffer = null;
	}
}
