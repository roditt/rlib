package net.site40.rodit.rlib.util.config;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.regex.Pattern;

import net.site40.rodit.rlib.util.io.StreamUtils;

public class Configuration {

	private LinkedHashMap<String, String> config;
	
	public Configuration(){
		this.config = new LinkedHashMap<String, String>();
	}
	
	public boolean isSet(String key){
		return config.containsKey(key);
	}
	
	public void set(String key, String value){
		config.put(key, value);
	}
	
	public String get(String key){
		String value = config.get(key);
		if(value == null)
			value = null;
		return value;
	}
	
	public void save(String file)throws IOException{
		save(new File(file));
	}
	
	public void save(File file)throws IOException{
		String full = "";
		for(String key : config.keySet())
			full += key + "=" + get(key) + "\n";
		StreamUtils.write(file, full.getBytes());
	}
	
	public static Configuration parse(String file)throws IOException{
		return parse(new File(file));
	}
	
	public static Configuration parse(File file)throws IOException{
		String[] parts = null;
		Configuration config = new Configuration();
		for(String line : StreamUtils.readAllLines(file)){
			if(line.startsWith("#"))
				continue;
			parts = line.split(Pattern.quote("="));
			if(parts.length < 2)
				continue;
			config.set(parts[0], parts[1]);
		}
		return  config;
	}
}
