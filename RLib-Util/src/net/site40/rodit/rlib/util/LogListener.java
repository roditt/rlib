package net.site40.rodit.rlib.util;

public interface LogListener {
	
	public static class LogListenerImpl implements LogListener{
		public boolean onInfo(String tag, String line){ return true; }
		public boolean onWarn(String tag, String line){ return true; }
		public boolean onError(String tag, String line){ return true; }
		public boolean onDebug(String tag, String line){ return true; }
	}

	public boolean onInfo(String tag, String line);
	public boolean onWarn(String tag, String line);
	public boolean onError(String tag, String line);
	public boolean onDebug(String tag, String line);
}
