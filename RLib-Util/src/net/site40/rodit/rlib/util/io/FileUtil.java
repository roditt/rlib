package net.site40.rodit.rlib.util.io;

import java.io.File;
import java.util.regex.Pattern;

public class FileUtil {

	public static String getExtension(File file){
		String[] parts = file.getName().split(Pattern.quote("."));
		if(parts.length == 1)
			return "";
		return parts[parts.length - 1];
	}
}
