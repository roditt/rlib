package net.site40.rodit.rlib.web;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;

import net.site40.rodit.rlib.util.AsyncCallback;
import net.site40.rodit.rlib.util.ByteArrayWriter;
import net.site40.rodit.rlib.util.Data;
import net.site40.rodit.rlib.util.io.StreamUtils;

public class WebClient {

	public static final String[] SUPPORTED_METHODS = new String[] { "GET", "POST", "PUT", "HEAD", "DELETE", "OPTIONS" };

	private HashMap<String, String> customHeaders;
	private HashMap<String, String> getParams;
	private HashMap<String, String> postParams;
	private boolean sendQueryStringForPostRequests;
	private boolean appendBodyToPostParams;
	private byte[] body;
	private int connectTimeout;
	private int readTimeout;

	private long requestStart;
	private long requestOpenStart;
	private long requestOpenEnd;
	private long requestReadWriteStart;
	private long requestEnd;

	public WebClient(){
		this.customHeaders = new HashMap<String, String>();
		this.getParams = new HashMap<String, String>();
		this.postParams = new HashMap<String, String>();
		this.sendQueryStringForPostRequests = true;
		this.appendBodyToPostParams = false;
		this.body = null;
		this.connectTimeout = 0;
		this.readTimeout = 0;
	}

	public void setHeader(String header, String value){
		customHeaders.put(header, value);
	}

	public void removeHeader(String header){
		customHeaders.remove(header);
	}

	public void addQuery(String name, String value){
		getParams.put(name, value);
	}

	public void removeQuery(String name){
		getParams.remove(name);
	}

	public void addPost(String name, String value){
		postParams.put(name, value);
	}

	public void removePost(String name){
		postParams.remove(name);
	}

	public boolean shouldSendQueryStringForPostRequests(){
		return sendQueryStringForPostRequests;
	}

	public void setSendQueryStringForPostRequests(boolean sendQueryStringForPostRequests){
		this.sendQueryStringForPostRequests = sendQueryStringForPostRequests;
	}

	public boolean shouldAppendBodyToPostParams(){
		return appendBodyToPostParams;
	}

	public void setAppendBodyToPostParams(boolean appendBodyToPostParams){
		this.appendBodyToPostParams = appendBodyToPostParams;
	}

	public byte[] getBody(){
		return body;
	}

	public void setBody(byte[] body){
		this.body = body;
	}

	public InputStream openStream(String url, String method)throws IOException{
		requestStart = System.currentTimeMillis();

		method = method.toUpperCase();
		if(!Data.arrayContains(SUPPORTED_METHODS, method))
			throw new IllegalArgumentException("Unsupported HTTP method '" + method + "'.");

		boolean writeData = method.equals("POST") || method.equals("PUT");
		if(getParams.size() > 0 && (method.equals("GET") || (writeData && sendQueryStringForPostRequests))){
			int getParamCount = 0;
			for(String key : getParams.keySet()){
				if(getParamCount == 0)
					url += "?";
				url += key + "=" + URLEncoder.encode(getParams.get(key), "UTF-8") + (getParamCount == getParams.size() - 1 ? "" : "&");
				getParamCount++;
			}
		}

		URL reqUrl = new URL(url);
		requestOpenStart = System.currentTimeMillis();
		HttpURLConnection connection = (HttpURLConnection)reqUrl.openConnection();
		requestOpenEnd = System.currentTimeMillis();

		byte[] toWrite = body;
		if(writeData && (appendBodyToPostParams || body == null) && postParams.size() > 0){
			String postParamsStr = "";
			int postParamCount = 0;
			for(String key : postParams.keySet()){
				postParamsStr += key + "=" + URLEncoder.encode(postParams.get(key), "UTF-8") + (postParamCount == postParams.size() - 1 ? "" : "&");
				postParamCount++;
			}
			ByteArrayWriter writer = new ByteArrayWriter();
			if(body != null)
				writer.write(body);
			if(postParamsStr.length() > 0)
				writer.write(postParamsStr);
			toWrite = writer.getBuffer();
			writer.dispose();
			writer = null;
		}
		connection.setRequestMethod(method);
		connection.setConnectTimeout(connectTimeout);
		connection.setReadTimeout(readTimeout);

		for(String key : customHeaders.keySet())
			connection.setRequestProperty(key, customHeaders.get(key));

		requestReadWriteStart = System.currentTimeMillis();
		connection.setDoInput(true);
		if(writeData){
			connection.setRequestProperty("Content-Length", String.valueOf(toWrite != null ? toWrite.length : 0));
			if(getParams.size() > 0)
				connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			connection.setDoOutput(true);
			if(toWrite != null)
				connection.getOutputStream().write(toWrite);
			toWrite = null;
			connection.getOutputStream().close();
		}

		InputStream stream = connection.getInputStream();

		requestEnd = System.currentTimeMillis();

		return stream;
	}
	
	public byte[] makeRequest(String url, String method)throws IOException{
		InputStream stream = openStream(url, method);
		byte[] read = StreamUtils.readAll(stream);
		stream.close();
		return read;
	}
	
	public void download(String url, String dstFile)throws IOException{
		InputStream in = openStream(url, "GET");
		FileOutputStream fout = new FileOutputStream(dstFile);
		StreamUtils.copy(in, fout);
		in.close();
		fout.close();
	}
	
	public void downloadAsync(final String url, final String dstFile, final AsyncCallback callback){
		new Thread(){
			public void run(){
				try{
					download(url, dstFile);
					if(callback != null)
						callback.callback(new Object[] { null, dstFile });
				}catch(IOException e){
					if(callback != null)
						callback.callback(new Object[] { e, null });
				}
			}
		}.start();
	}
	
	public long getOpenTime(){
		return requestOpenEnd - requestOpenStart;
	}
	
	public long getReadWriteTime(){
		return requestEnd - requestReadWriteStart;
	}

	public long getTotalRequestTime(){
		return requestEnd - requestStart;
	}
}
