package net.site40.rodit.rlib.sockets.tcp.server;

import net.site40.rodit.rlib.sockets.tcp.DisconnectReason;

/**
 * The server listener interface is used as a callback for certain events in the server's lifetime.
 */
public interface IServerListener {

	/**
	 * Called when a client successfully connects to the server.
	 * @param connection The connection holding the remote socket with which the server has just established a connection.
	 */
	public void onConnected(Connection connection);
	/**
	 * Called when a client disconnected from the server.
	 * @param connection The connection holding the socket with which the server has just disconnected from.
	 * @param reason The reason for the disconnection.
	 */
	public void onDisconnected(Connection connection, DisconnectReason reason);
	/**
	 * Called when the server receives data from a client.
	 * @param connection The connection from which the data has been received.
	 * @param data The buffer of data which has been read from the connection's socket input stream.
	 */
	public void onDataReceived(Connection connection, byte[] data);
	/**
	 * Called when the server encounters a non-fatal, unhandled exception.
	 * @param e The exception which has not been handled by the server.
	 */
	public void onServerException(Exception e);
}
