package net.site40.rodit.rlib.util;

import net.site40.rodit.rlib.util.LogListener.LogListenerImpl;


public class Log {

	public static final int NONE = -1;
	public static final int INFO = 0;
	public static final int ERROR = 1;
	public static final int WARN = 2;
	public static final int VERBOSE = 3;

	private static int level = VERBOSE;
	private static LogListener listener = new LogListenerImpl();
	
	public static void setListener(LogListener listener){
		Log.listener = listener;
	}

	public static void setLevel(int level){
		Log.level = level;
	}

	public static void i(String tag, String info){
		if(level >= INFO && listener.onInfo(tag, info))
			System.out.println("[INF][" + tag + "]: " + info);
	}

	public static void e(String tag, String info){
		if(level >= ERROR && listener.onError(tag, info))
			System.err.println("[ERR][" + tag + "]: " + info);
	}

	public static void w(String tag, String info){
		if(level >= WARN && listener.onWarn(tag, info))
			System.out.println("[WRN][" + tag + "]: " + info);
	}

	public static void v(String tag, String info){
		if(level >= VERBOSE && listener.onDebug(tag, info))
			System.out.println("[DBG][" + tag + "]: " + info);
	}
}
