package net.site40.rodit.rlib.util;

import java.util.Map;

public class CollectionsUtil {

	public static <T>T getMapKeyByIndex(Map<T, ?> map, int index){
		int cIndex = 0;
		for(T key : map.keySet()){
			if(cIndex == index)
				return key;
			cIndex++;
		}
		return null;
	}
}
