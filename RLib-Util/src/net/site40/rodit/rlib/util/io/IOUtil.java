package net.site40.rodit.rlib.util.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class IOUtil {

	public static boolean deleteDir(File dir)throws IOException{
		return deleteDir(dir, true);
	}
	
	public static boolean deleteDir(File dir, boolean recursive)throws IOException{
		if(!dir.isDirectory())
			throw new IOException("Cannot delete non-existing directory.");
		if(!recursive)
			return dir.delete();
		else{
			boolean success = true;
			for(File f : dir.listFiles()){
				if(f.isDirectory())
					if(!deleteDir(f, true))
						success = false;
				else
					if(!f.delete())
						success = false;
			}
			return success;
		}
	}
	
	public static void secureDelete(File file)throws IOException{
		secureDelete(file, 1024 * 1024);
	}
	
	public static void secureDelete(File file, int writeBufferSize)throws IOException{
		if(!file.isFile())
			throw new IOException("Cannot delete non-existing file.");
		long fileSizeL = file.length();
		if(fileSizeL > Integer.MAX_VALUE)
			throw new IOException("Cannot securely delete file with size greater than " + Integer.MAX_VALUE + "B.");
		int fileSize = (int)fileSizeL;
		FileOutputStream fout = new FileOutputStream(file);
		byte[] writeBuffer = new byte[writeBufferSize];
		int written = 0;
		while(written < fileSize){
			if(writeBuffer.length > fileSize - written)
				writeBuffer = new byte[fileSize - written];
			fout.write(writeBuffer);
			fout.flush();
			written += writeBuffer.length;
		}
		fout.close();
		file.delete();
	}
}
