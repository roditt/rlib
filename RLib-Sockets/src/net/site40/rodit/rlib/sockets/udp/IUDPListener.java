package net.site40.rodit.rlib.sockets.udp;

import java.net.DatagramPacket;

public interface IUDPListener {

	public void onDataReceived(DatagramPacket packet);
	public void onException(DatagramPacket packet, Exception e);
}
