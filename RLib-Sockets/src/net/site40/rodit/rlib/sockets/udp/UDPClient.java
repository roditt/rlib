package net.site40.rodit.rlib.sockets.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class UDPClient {

	public static final long DEFAULT_READ_SLEEP_TIME = 25L;
	public static final int DEFAULT_READ_BUFFER_SIZE = 4096;

	private DatagramSocket socket;
	private DatagramPacket packet;
	private byte[] readBuffer;
	private long sleepTime;

	private boolean running;
	private Thread thread;

	private List<IUDPListener> listeners;
	
	public UDPClient()throws IOException{
		this(0);
	}

	public UDPClient(int port)throws IOException{
		this(port, DEFAULT_READ_BUFFER_SIZE);
	}

	public UDPClient(int port, int bufferSize)throws IOException{
		this(port, bufferSize, DEFAULT_READ_SLEEP_TIME);
	}

	public UDPClient(int port, int bufferSize, long sleepTime)throws IOException{
		this.socket = new DatagramSocket(port);
		this.readBuffer = new byte[bufferSize];
		this.sleepTime = sleepTime;
		this.listeners = Collections.synchronizedList(new ArrayList<IUDPListener>());
	}
	
	public DatagramSocket getSocket(){
		return socket;
	}

	public boolean isOpen(){
		return socket != null && !socket.isClosed();
	}

	public void addListener(IUDPListener listener){
		if(!listeners.contains(listener))
			listeners.add(listener);
	}

	public void removeListener(IUDPListener listener){
		listeners.remove(listener);
	}

	protected void onDataReceived(DatagramPacket packet){
		for(IUDPListener listener : listeners)
			listener.onDataReceived(packet);
	}

	protected void onException(DatagramPacket packet, Exception e){
		for(IUDPListener listener : listeners)
			listener.onException(packet, e);
	}

	public void write(String host, int port, byte[] data){
		write(new InetSocketAddress(host, port), data);
	}

	public void write(InetSocketAddress address, byte[] data){
		try{
			DatagramPacket packet = new DatagramPacket(data, data.length, address);
			socket.send(packet);
			packet = null;
		}catch(IOException e){
			onException(packet, e);
		}
	}

	public void start(){
		if(running)
			throw new IllegalStateException("Cannot start server when already running.");
		else{
			thread = new Thread(){
				@Override
				public void run(){
					running = true;
					while(running && isOpen()){
						packet = new DatagramPacket(readBuffer, readBuffer.length);
						try{
							socket.receive(packet);
							if(packet.getLength() > 0)
								onDataReceived(packet);
						}catch(IOException e){
							onException(packet, e);
						}
						packet = null;
						
						try{
							if(sleepTime > 0L)
								Thread.sleep(sleepTime);
						}catch(InterruptedException e){}
					}
				}
			};
			thread.start();
		}
	}

	public void stop(){
		if(!isOpen() || !running)
			throw new IllegalStateException("Cannot stop server when it's not running.");
		else{
			running = false;
			socket.close();
		}
	}
}
