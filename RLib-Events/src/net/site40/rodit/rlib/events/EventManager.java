package net.site40.rodit.rlib.events;

import java.util.ArrayList;
import java.util.HashMap;

public class EventManager {

	private HashMap<EventIdentifier, ArrayList<IEventHandler<?>>> eventHandlers;

	public EventManager(){
		this.eventHandlers = new HashMap<EventIdentifier, ArrayList<IEventHandler<?>>>();
	}

	public ArrayList<IEventHandler<?>> getAllHandlers(EventIdentifier id){
		ArrayList<IEventHandler<?>> handlers = eventHandlers.get(id);
		if(handlers == null)
			eventHandlers.put(id, handlers = new ArrayList<IEventHandler<?>>());
		return handlers;
	}

	public void register(EventIdentifier id, IEventHandler<?> handler){
		ArrayList<IEventHandler<?>> handlers = getAllHandlers(id);
		if(!handlers.contains(handler))
			handlers.add(handler);
	}

	public void unregister(EventIdentifier id, IEventHandler<?> handler){
		getAllHandlers(id).remove(handler);
	}

	public void unregisterFromAll(IEventHandler<?> handler){
		for(EventIdentifier id : eventHandlers.keySet())
			unregister(id, handler);
	}

	@SuppressWarnings("unchecked")
	public <T> void triggerEvent(EventIdentifier id, T args){
		for(IEventHandler<?> handler : getAllHandlers(id))
			((IEventHandler<T>)handler).onEvent(args);
	}
}
