package net.site40.rodit.rlib.events;

public interface IEventHandler<T> {

	public void onEvent(T args);
}
