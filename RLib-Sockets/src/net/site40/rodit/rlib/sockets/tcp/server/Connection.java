package net.site40.rodit.rlib.sockets.tcp.server;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.regex.Pattern;

import net.site40.rodit.rlib.sockets.tcp.DisconnectReason;
import net.site40.rodit.rlib.util.AsyncCallback;
import net.site40.rodit.rlib.util.Data;
import net.site40.rodit.rlib.util.Log;

public class Connection {
	
	/**
	 * The default size of the buffer used to read from the socket input stream.
	 */
	public static final int DEFAULT_READ_BUFFER_SIZE = 4096;

	private Server server;
	private Socket socket;

	private int connectionId;
	private long readSleepTime;
	private Thread thread;
	private boolean closed = false;

	private InputStream in;
	private OutputStream out;

	private ByteArrayOutputStream currentRead;
	private byte[] readBuffer;

	private int waitBufferExpectedLength = 0;
	private byte[] waitBuffer = new byte[0];

	private AsyncCallback runOnReadThread;

	/**
	 * Creates a new Connection with the specified options. The default read buffer size will be used.
	 * @param server The server which is the parent to this connection.
	 * @param socket The remote socket which is accepted by the {@link ServerSocket#accept()} method.
	 * @param connectionId The id to assign to this connection.
	 * @param readSleepTime The time the connection thread sleeps between each consecutive read of the socket input stream.
	 * @see #DEFAULT_READ_BUFFER_SIZE
	 */
	public Connection(Server server, Socket socket, int connectionId, long readSleepTime){
		this(server, socket, connectionId, readSleepTime, DEFAULT_READ_BUFFER_SIZE);
	}


	/**
	 * Creates a new Connection with the specified options.
	 * @param server The server which is the parent to this connection.
	 * @param socket The remote socket which is accepted by the {@link ServerSocket#accept()} method.
	 * @param connectionId The id to assign to this connection.
	 * @param readSleepTime The time the connection thread sleeps between each consecutive read of the socket input stream.
	 * @param readBufferSize The size of the buffer used to read from the socket input stream.
	 */
	public Connection(Server server, Socket socket, int connectionId, long readSleepTime, int readBufferSize){
		this.server = server;
		this.socket = socket;

		this.connectionId = connectionId;
		this.readSleepTime = readSleepTime;

		this.readBuffer = new byte[readBufferSize];
	}
	
	/**
	 * Get the connection's working thread.
	 * @return The connection's thread which is used to read data from the socket and detect disconnects and exceptions.
	 */
	public Thread getThread(){
		return thread;
	}

	/**
	 * Sets a runnable who's {@link Runnable#run()} will be run on every loop of the connection's thread.
	 * This can be set to null to stop any runnable from being run in the connection's thread's loop.
	 * @param runnable The runnable who's {@link Runnable#run()} method will be run on the connection's thread.
	 */
	public void runOnReadThread(AsyncCallback runOnReadThread){
		this.runOnReadThread = runOnReadThread;
	}

	protected void initThread()throws IOException{
		this.in = socket.getInputStream();
		this.out = socket.getOutputStream();

		thread = new Thread(){
			@Override
			public void run(){
				boolean errorClosed = false;
				while(isConnected()){
					try{
						currentRead = new ByteArrayOutputStream();
						int read = 0;
						while(in.available() > 0 && (read = in.read(readBuffer)) > 0)
							currentRead.write(readBuffer, 0, read);

						if(read == -1 || in.available() == -1)
							throw new IOException("Client socket closed.");

						if(currentRead.size() > 0){
							byte[] cBuffer = currentRead.toByteArray();
							currentRead.close();
							currentRead = null;
							read = 0;
							while(read < cBuffer.length){
								if(waitBuffer != null && waitBufferExpectedLength > 0 && waitBufferExpectedLength - waitBuffer.length > 0){
									int remainWaitBufferLength = waitBufferExpectedLength - waitBuffer.length;
									int toWrite = remainWaitBufferLength > cBuffer.length ? cBuffer.length : remainWaitBufferLength;
									byte[] waitBufferAdd = new byte[toWrite];
									System.arraycopy(cBuffer, 0, waitBufferAdd, 0, toWrite);
									waitBuffer = Data.concat(waitBuffer, waitBufferAdd);
									if(waitBuffer.length == waitBufferExpectedLength){
										server.onDataReceived(Connection.this, waitBuffer);
										waitBuffer = null;
										break;
									}
									if(cBuffer.length - toWrite > 0){
										byte[] ncbuffer = new byte[cBuffer.length - toWrite];
										System.arraycopy(cBuffer, toWrite, ncbuffer, 0, cBuffer.length - toWrite);
										cBuffer = ncbuffer;
									}else
										break;
								}

								byte[] pLengthData = new byte[4];
								System.arraycopy(cBuffer, read, pLengthData, 0, 4);
								int pLength = Data.convertInt(pLengthData);
								pLengthData = null;
								read += 4;

								if(pLength > cBuffer.length){
									waitBuffer = cBuffer;
									waitBufferExpectedLength = pLength;
									break;
								}

								if(pLength < 0){
									Log.e("NAS", "len cBuffer=" + cBuffer.length + " read=" + read + " pLength=" + pLength + ". The packet will be dropped.");
									waitBuffer = null;
									waitBufferExpectedLength = 0;
									break;
								}
								byte[] packet = new byte[pLength];
								if(pLength > cBuffer.length - read){
									Log.e("IOBE", "len cBuffer=" + cBuffer.length + " read=" + read + " packet=" + packet.length + " pLength=" + pLength + ". The packet will be dropped.");
									waitBuffer = null;
									waitBufferExpectedLength = 0;
									packet = null;
									break;
								}else
									System.arraycopy(cBuffer, read, packet, 0, pLength);
								server.onDataReceived(Connection.this, packet);
								packet = null;
								read += pLength;
							}
							cBuffer = null;
						}
					}catch(IOException e){
						if(isConnected()){
							try{
								socket.close();
							}catch(IOException e0){}
						}
						e.printStackTrace();
						errorClosed = true;
						closed = true;
						server.onDisconnected(Connection.this, DisconnectReason.CONNECTION_CLOSED);
						//TODO: Correct reason of disconnection.
					}

					if(runOnReadThread != null)
						runOnReadThread.callback(new Object[] { Connection.this });

					try{
						if(readSleepTime > 0L)
							Thread.sleep(readSleepTime);
					}catch(InterruptedException e){}
				}
				if(!errorClosed)
					server.onDisconnected(Connection.this, DisconnectReason.CLIENT_DISCONNECTED);
				closed = true;
			}
		};
		thread.start();
	}

	/**
	 * Gets the server which is the parent to this connection.
	 * @return The server to which this connection is connected to.
	 */
	public Server getServer(){
		return server;
	}

	/**
	 * Gets the remote socket of this connection.
	 * @return The remote socket connected to the parent server.
	 */
	public Socket getSocket(){
		return socket;
	}

	/**
	 * Gets the readable IP address (host:port) of this connection's remote socket.
	 * @return The readable IP address of the remote socket.
	 */
	public String getIP(){
		return socket.getRemoteSocketAddress().toString().split(Pattern.quote(":"))[0].replace("\\", "").replace("/", "");
	}

	/**
	 * Gets the port of this connection's remote socket.
	 * @return The port of the remote socket.
	 */
	public int getPort(){
		return socket.getPort();
	}

	/**
	 * Gets the connection ID assigned to this connection in it's constructor.
	 * @return This connection's connection ID.
	 */
	public int getConnectionId(){
		return connectionId;
	}

	/**
	 * Checks whether the connection's socket is connected.
	 * @return Whether the connection's socket is connected.
	 */
	public boolean isConnected(){
		return socket != null && socket.isConnected() && !socket.isClosed() && !closed;
	}
	
	/**
	 * Closes this client's socket and suppresses any IOExceptions.
	 * @return Whether the client socket was closed successfully.
	 */
	public boolean quietDisconnect(){
		try{
			socket.close();
			return true;
		}catch(IOException e){
			return false;
		}
	}
	
	/**
	 * Writes the specified data to the socket output stream.
	 * @param data The buffer to be written to the socket output stream.
	 * @throws IOException Passes any IOException thrown by the {@link OutputStream#write(byte[])} method.
	 */
	public void write(byte[] data){
		try{
			byte[] full = Data.concat(Data.convertBytes(data.length), data);
			writeRaw(full);
			full = null;
		}catch(IOException e){
			if(e.getMessage().toLowerCase().contains("broken pipe") || e.getMessage().toLowerCase().contains("connection reset"))
				server.onDisconnected(this, DisconnectReason.CLIENT_DISCONNECTED);
			closed = true;
		}
	}

	protected void writeRaw(byte[] data)throws IOException{
		out.write(data);
		out.flush();
	}
}
