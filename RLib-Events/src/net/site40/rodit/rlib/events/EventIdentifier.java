package net.site40.rodit.rlib.events;

public class EventIdentifier {

	private Object id;
	
	public EventIdentifier(Object id){
		this.id = id;
	}
	
	public Object getId(){
		return id;
	}
	
	public void setId(Object id){
		this.id = id;
	}
	
	@Override
	public int hashCode(){
		return id != null ? id.hashCode() : super.hashCode();
	}
	
	@Override
	public boolean equals(Object object){
		return object instanceof EventIdentifier && ((EventIdentifier)object).id.equals(id);
	}
}
