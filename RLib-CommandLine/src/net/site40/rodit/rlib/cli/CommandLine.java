package net.site40.rodit.rlib.cli;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.regex.Pattern;

public class CommandLine {

	private String name;
	private ArrayList<CommandLineOption> options;

	public CommandLine(String name){
		this.name = name;
		this.options = new ArrayList<CommandLineOption>();
	}

	public String getName(){
		return name;
	}

	public void setName(String name){
		this.name = name;
	}
	public CommandLineOption getOption(String token){
		for(CommandLineOption option : options)
			if(option.getTokens().contains(token))
				return option;
		return null;
	}

	public void addOption(String token, String usage, boolean hasValue, boolean required, String... aliases){
		addOption(new CommandLineOption(token, usage, hasValue, required, aliases));
	}

	public void addOption(CommandLineOption option){
		options.add(option);
	}

	public void printHelp(){
		printHelp(System.out);
	}

	public void printHelp(PrintStream stream){
		stream.println("Help for " + name + ":\n");
		for(CommandLineOption option : options)
			option.printUsage(stream);
	}

	public CommandLineEnvironment parse(String[] args){
		CommandLineEnvironment environment = new CommandLineEnvironment(this);
		String currentToken = "";
		for(int i = 0; i < args.length; i++){
			String arg = args[i];
			CommandLineOption option = getOption(currentToken.replaceFirst(Pattern.quote("-"), ""));
			if(currentToken.length() == 0 || (option != null && !option.hasValue())){
				currentToken = arg.trim();
				if(option == null)
					option = getOption(currentToken.replaceFirst(Pattern.quote("-"), ""));
				environment.setValue(option, "");
				if(option != null && !option.hasValue())
					currentToken = "";
			}else if(option != null && option.hasValue()){
				environment.setValue(option, arg);
				currentToken = "";
			}else if(option == null){
				System.out.println("Invalid token " + currentToken + ".");
				environment.setHasError(true);
				printHelp();
			}
		}
		environment.setHasRequired(true);
		for(CommandLineOption option : options){
			if(option.isRequired() && !environment.hasValue(option)){
				System.out.println("Missing required token " + option.getToken() + ".");
				option.printUsage();
				environment.setHasRequired(false);
				break;
			}
		}
		return environment;
	}
}
