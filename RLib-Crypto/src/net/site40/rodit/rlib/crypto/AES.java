package net.site40.rodit.rlib.crypto;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AES {
	
	public static final byte[] DEFAULT_KEY = new byte[] { 84, 2, 94, 30, 13, -42, 1, -34, 9, -1, -42, 84, 10, -33, 93, 11 };
	public static final byte[] DEFAULT_IV = new byte[] { 93, 124, 51, 50, -31, 41, 121, 48, -22, -49, 40, -1, 52, 99, -13, 4 };

	public static byte[] encryptUnsafe(byte[] data, byte[] key) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException{
		return transform(Cipher.ENCRYPT_MODE, key, DEFAULT_IV, data);
	}
	
	public static byte[] encryptUnsafe(byte[] data, byte[][] keys) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException{
		byte[] encrypted = data;
		for(int i = 0; i < keys.length; i++){
			byte[] key = keys[i];
			encrypted = encryptUnsafe(encrypted, key);
		}
		return encrypted;
	}
	
	public static byte[] decryptUnsafe(byte[] data, byte[] key) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException{
		return transform(Cipher.DECRYPT_MODE, key, DEFAULT_IV, data);
	}
	
	public static byte[] decryptUnsafe(byte[] data, byte[][] keys) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException{
		byte[] decrypted = data;
		for(int i = 0; i < keys.length; i++){
			byte[] key = keys[keys.length - i - 1];
			decrypted = decryptUnsafe(decrypted, key);
		}
		return decrypted;
	}
	
	public static byte[] encrypt(byte[] data, byte[] key){
		try{
			return encryptUnsafe(data, key);
		}catch(Exception e){
			e.printStackTrace();
		}
		return new byte[0];
	}

	public static byte[] decrypt(byte[] data, byte[] key){
		try{
			return decryptUnsafe(data, key);
		}catch(Exception e){
			e.printStackTrace();
		}
		return new byte[0];
	}
	
	public static byte[] encrypt(byte[] data, byte[][] keys){
		byte[] encrypted = data;
		for(int i = 0; i < keys.length; i++){
			byte[] key = keys[i];
			encrypted = encrypt(encrypted, key);
		}
		return encrypted;
	}
	
	public static byte[] decrypt(byte[] data, byte[][] keys){
		byte[] decrypted = data;
		for(int i = 0; i < keys.length; i++){
			byte[] key = keys[keys.length - i - 1];
			decrypted = decrypt(decrypted, key);
		}
		return decrypted;
	}
	
	private static byte[] transform(final int mode, final byte[] keyBytes, final byte[] ivBytes, final byte[] messageBytes)throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException{
		if(messageBytes.length == 0)
			return messageBytes;
		final SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
		final IvParameterSpec ivSpec = new IvParameterSpec(ivBytes);
		final Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

		cipher.init(mode, keySpec, ivSpec);

		return cipher.doFinal(messageBytes);
	}
}
