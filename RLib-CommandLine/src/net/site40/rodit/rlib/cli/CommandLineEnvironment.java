package net.site40.rodit.rlib.cli;

import java.util.HashMap;

public class CommandLineEnvironment {

	private CommandLine cli;
	private HashMap<CommandLineOption, String> args;
	private boolean hasRequired;
	private boolean hasError;
	
	public CommandLineEnvironment(CommandLine cli){
		this.cli = cli;
		this.args = new HashMap<CommandLineOption, String>();
		this.hasRequired = false;
	}
	
	public CommandLine getCli(){
		return  cli;
	}
	
	public boolean hasValue(String token){
		return hasValue(cli.getOption(token));
	}
	
	public boolean hasValue(CommandLineOption option){
		return args.containsKey(option);
	}
	
	public String getValue(String token){
		return getValue(cli.getOption(token));
	}
	
	public String getValue(CommandLineOption option){
		return args.get(option);
	}
	
	public String getValueOrDefault(String token, String value){
		return getValueOrDefault(cli.getOption(token), value);
	}
	
	public String getValueOrDefault(CommandLineOption option, String value){
		return hasValue(option) ? getValue(option) : value;
	}
	
	public void setValue(String token, String value){
		setValue(cli.getOption(token), value);
	}
	
	public void setValue(CommandLineOption option, String value){
		args.put(option, value);
	}
	
	public boolean hasRequired(){
		return hasRequired;
	}
	
	public void setHasRequired(boolean hasRequired){
		this.hasRequired = hasRequired;
	}
	
	public boolean hasError(){
		return hasError;
	}
	
	public void setHasError(boolean hasError){
		this.hasError = hasError;
	}
}
