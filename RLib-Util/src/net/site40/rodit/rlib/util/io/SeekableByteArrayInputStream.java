package net.site40.rodit.rlib.util.io;

import java.io.ByteArrayInputStream;

public class SeekableByteArrayInputStream extends ByteArrayInputStream {

	public SeekableByteArrayInputStream(byte[] data){
		super(data);
	}
	
	public void setPosition(int position){
		this.pos = position;
	}
	
	public void skip(int skip){
		this.pos += skip;
	}
}
