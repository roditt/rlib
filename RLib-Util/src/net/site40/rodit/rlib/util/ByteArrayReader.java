package net.site40.rodit.rlib.util;

/**
 * A utility class to read different data types from a byte array.
 */
public class ByteArrayReader {

	private byte[] buffer;
	private int offset;
	
	/**
	 * Initializes a new {@link #ByteArrayReader} with the specified buffer.
	 * @param buffer The buffer to read from.
	 */
	public ByteArrayReader(byte[] buffer){
		this.buffer = buffer;
		this.offset = 0;
	}
	
	/**
	 * Get the buffer currently being read from.
	 * @return The buffer currently being read from.
	 */
	public byte[] getBuffer(){
		return buffer;
	}
	
	/**
	 * Set the buffer currently being read from.
	 * <br>
	 * <br>
	 * <b>Note:</b> This does not reset the current read offset.
	 * @param buffer The buffer to start reading from.
	 */
	public void setBuffer(byte[] buffer){
		this.buffer = buffer;
	}
	
	/**
	 * Set the current position in the byte array to read from.
	 * @param offset The position in the byte array to set the current offset to.
	 */
	public void offset(int offset){
		this.offset = offset;
		if(offset >= buffer.length)
			offset = 0;
		if(offset < 0)
			offset = buffer.length - 1;
	}
	
	/**
	 * Increases the current offset by the specified amount.
	 * @param amount The amount to increase the offset by.
	 */
	public void inc(int amount){
		offset(offset + amount);
	}

	/**
	 * Decreases the current offset by the specified amount.
	 * @param amount The amount to decrease the offset by.
	 */
	public void dec(int amount){
		offset(offset - amount);
	}
	
	/**
	 * Get the amount of bytes that can still be read in the byte array until the offset resets to 0.
	 * @return The amount of bytes that remain in the byte array.
	 */
	public int remaining(){
		return buffer.length - offset;
	}
	
	/**
	 * Reads the specified amount of bytes from the byte array.
	 * @param length The amount of bytes to be read.
	 * @return The bytes from the offset to the length.
	 */
	public byte[] read(int length){
		byte[] data = Data.read(buffer, offset, length);
		inc(length);
		return data;
	}
	
	/**
	 * Reads the next byte in the byte array.
	 * @return The next byte in the byte array.
	 */
	public byte read(){
		return read(1)[0];
	}

	/**
	 * Reads the next int in the byte array.
	 * @return The next int in the byte array.
	 */
	public int readInt(){
		return Data.convertInt(read(4));
	}

	/**
	 * Reads the next long in the byte array.
	 * @return The next long in the byte array.
	 */
	public long readLong(){
		return Data.convertLong(read(8));
	}

	/**
	 * Reads the next float in the byte array.
	 * @return The next float in the byte array.
	 */
	public float readFloat(){
		return Data.convertFloat(read(4));
	}

	/**
	 * Reads the next double in the byte array.
	 * @return The next double in the byte array.
	 */
	public double readDouble(){
		return Data.convertDouble(read(8));
	}

	/**
	 * Reads the next boolean in the byte array.
	 * @return The next boolean in the byte array.
	 */
	public boolean readBool(){
		return Data.convertBool(read(1));
	}

	/**
	 * Reads the next string in the byte array. This method assumes the string's length is stored in the next int in the byte array.
	 * @return The next string in the byte array.
	 */
	public String readString(){
		return readString(readInt());
	}

	/**
	 * Reads the next string in the byte array with the specified length.
	 * @return The next string in the byte array.
	 */
	public String readString(int length){
		return new String(read(length));
	}
	
	/**
	 * Disposes of the current buffer by setting its reference to null.
	 */
	public void dispose(){
		buffer = null;
	}
}
