package net.site40.rodit.rlib.sockets.tcp.client;

import net.site40.rodit.rlib.sockets.tcp.DisconnectReason;

/**
 * The client listener interface is used as a callback for certain events in the client's lifetime.
 */
public interface IClientListener {

	/**
	 * Called when a client successfully connects to a host.
	 * @param client The client calling the {@link #onConnected(Client)} method.
	 */
	public void onConnected(Client client);
	/**
	 * Called when a client is disconnected from its previously connected host.
	 * @param client The client calling the {@link #onDisconnected(Client, DisconnectReason)} method.
	 * @param reason The reason the client was disconnected.
	 * @see #DisconnectReason
	 */
	public void onDisconnected(Client client, DisconnectReason reason);
	/**
	 * Called when a client successfully reads a non-zero length buffer from the socket's input stream.
	 * @param client The client calling the {@link #onDataReceived(Client, byte[])} method.
	 * @param data The buffer of data read from the input stream of the client socket.
	 */
	public void onDataReceived(Client client, byte[] data);
	/**
	 * Called when a client encounters a non-fatal, unhandled exception.
	 * @param client The client calling the {@link #onClientException(Client, Exception)} method.
	 * @param e The exception which was not handled by the client.
	 */
	public void onClientException(Client client, Exception e);
}
