package net.site40.rodit.rlib.util.io;

import java.io.IOException;
import java.io.InputStream;

public class LimitedStream extends InputStream{
	
	private InputStream in;
	private int maxRead;
	private int read;
	
	public LimitedStream(InputStream in, int maxRead){
		this.in = in;
		this.maxRead = maxRead;
	}

	@Override
	public int read()throws IOException{
		if(read >= maxRead)
			return -1;
		read++;
		return in.read();
	}
	
	public void readAvailable()throws IOException{
		while(read < maxRead){
			read();
		}
	}
}
