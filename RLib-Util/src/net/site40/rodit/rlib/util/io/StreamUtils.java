package net.site40.rodit.rlib.util.io;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public class StreamUtils {
	
	public static final int DEFAULT_READ_BUFFER_SIZE = 8192;
	
	public static byte[] read(InputStream stream, int length)throws IOException{
		byte[] buffer = new byte[length];
		if(stream.read(buffer) > 0)
			return buffer;
		return buffer = null;
	}
	
	public static byte[] readAll(InputStream stream)throws IOException{
		return readAll(stream, DEFAULT_READ_BUFFER_SIZE);
	}
	
	public static byte[] readAll(InputStream stream, int bufferSize)throws IOException{
		byte[] readBuffer = new byte[bufferSize];
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		int read = 0;
		while((read = stream.read(readBuffer)) > 0)
			out.write(readBuffer, 0, read);
		readBuffer = null;
		byte[] data = out.toByteArray();
		out.close();
		out = null;
		return data;
	}
	
	public static byte[] readFile(String file)throws IOException{
		return readFile(new File(file));
	}
	
	public static byte[] readFile(File file)throws IOException{
		FileInputStream fin = new FileInputStream(file);
		byte[] data = readAll(fin);
		fin.close();
		return data;
	}
	
	public static String readFileAsString(String file)throws IOException{
		return readFileAsString(new File(file));
	}
	
	public static String readFileAsString(File file)throws IOException{
		return new String(readFile(file));
	}
	
	public static void copy(InputStream in, OutputStream out)throws IOException{
		copy(in, out, DEFAULT_READ_BUFFER_SIZE);
	}
	
	public static void copy(InputStream in, OutputStream out, int bufferSize)throws IOException{
		byte[] buffer = new byte[bufferSize];
		int read = 0;
		while((read = in.read(buffer)) > 0)
			out.write(buffer, 0, read);
		buffer = null;
	}
	
	public static void copyAndClose(InputStream in, OutputStream out)throws IOException{
		copyAndClose(in, out, DEFAULT_READ_BUFFER_SIZE);
	}
	
	public static void copyAndClose(InputStream in, OutputStream out, int bufferSize)throws IOException{
		copy(in, out, bufferSize);
		in.close();
		out.close();
	}
	
	public static boolean safeClose(InputStream stream){
		try{
			stream.close();
			return true;
		}catch(IOException e){ return false; }
	}
	
	public static boolean safeClose(OutputStream out){
		try{
			out.close();
			return true;
		}catch(IOException e){ return false; }
	}
	
	public static String[] readAllLines(String file)throws IOException{
		return readAllLines(new File(file));
	}
	
	public static String[] readAllLines(File file)throws IOException{
		BufferedReader reader = new BufferedReader(new FileReader(file));
		ArrayList<String> lines = new ArrayList<String>();
		String line = null;
		while((line = reader.readLine()) != null)
			lines.add(line);
		reader.close();
		String[] allLines = lines.toArray(new String[0]);
		lines = null;
		return allLines;
	}
	
	public static void write(String file, byte[] data)throws IOException{
		write(new File(file), data);
	}
	
	public static void write(File file, byte[] data)throws IOException{
		write(file, data, false);
	}
	
	public static void write(String file, byte[] data, boolean append)throws IOException{
		write(new File(file), data, append);
	}
	
	public static void write(File file, byte[] data, boolean append)throws IOException{
		FileOutputStream fout = new FileOutputStream(file, append);
		fout.write(data);
		fout.flush();
		fout.close();
	}
}
