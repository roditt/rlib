package net.site40.rodit.rlib.sockets.tcp.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.net.ServerSocketFactory;

import net.site40.rodit.rlib.sockets.tcp.DisconnectReason;

/**
 * This class wraps around a Java {@link #ServerSocket}, allowing simplified communication to multiple sockets.
 * @see #Connection
 */
public class Server {

	/**
	 * The default time for the server thread to sleep between each successful {@link ServerSocket#accept()} call.
	 */
	public static final long DEFAULT_SLEEP_TIME = 50L;
	/**
	 * The default time for connection threads to sleep between each consecutive read of the socket input stream.
	 */
	public static final long DEFAULT_CONNECTION_READ_SLEEP_TIME = 25L;

	private int port;
	private long sleepTime;
	private long connectionReadSleepTime;
	private boolean running;

	private List<Connection> connections;
	private List<IServerListener> listeners;

	private ServerSocket serverSocket;
	private Thread thread;

	private int connectionCounter;
	
	/**
	 * Creates a new server and underlying {@link #ServerSocket} with the specified port and the default server and connection sleep times.
	 * @param port The port to bind the {@link #ServerSocket} to.
	 * @see #DEFAULT_SLEEP_TIME
	 * @see #DEFAULT_CONNECTION_READ_SLEEP_TIME
	 */
	public Server(int port){
		this(port, DEFAULT_SLEEP_TIME, DEFAULT_CONNECTION_READ_SLEEP_TIME);
	}

	/**
	 * Creates a new server and underlying {@link #ServerSocket} with the specified port, sleep time and connection read sleep time.
	 * @param port The port to bind the {@link #ServerSocket} to.
	 * @param sleepTime The time for the server thread to sleep between each successful {@link ServerSocket#accept()} call.
	 * @param connectionReadSleepTime The time for connection threads to sleep between each consecutive read of the socket input stream.
	 */
	public Server(int port, long sleepTime, long connectionReadSleepTime){
		this.port = port;
		this.sleepTime = sleepTime;
		this.connectionReadSleepTime = connectionReadSleepTime;
		this.running = false;

		this.connections = Collections.synchronizedList(new ArrayList<Connection>());
		this.listeners = Collections.synchronizedList(new ArrayList<IServerListener>());

		this.connectionCounter = 0;
	}

	/**
	 * Gets the port to which the current server was initially bound.
	 * @return The port to which the current server was initially bound.
	 */
	public int getPort(){
		return port;
	}

	/**
	 * Sets the port to bind the {@link #SocketServer} to when the server is started.
	 * <br>
	 * <br>
	 * <b>Note:</b> This will have no effect if the server has already been started.
	 * @param port The port to bind the {@link #SocketServer} to.
	 */
	public void setPort(int port){
		this.port = port;
	}

	/**
	 * Gets the server connection sleep time.
	 * @return The server connection sleep time.
	 */
	public long getSleepTime(){
		return sleepTime;
	}

	/**
	 * Sets the server connection sleep time to the specified sleep time.
	 * @param sleepTime The new server connection sleep time.
	 */
	public void setSleepTime(long sleepTime){
		this.sleepTime = sleepTime;
	}

	/**
	 * Checks whether the server is currently running and listening for connections.
	 * @return Whether the server is currently running.
	 */
	public boolean isRunning(){
		return running;
	}

	/**
	 * Gets a list of the current connections to the server. The returned list should not be modified.
	 * @return A list of the current connections to the server.
	 */
	public List<Connection> getConnections(){
		return connections;
	}

	protected void addConnection(Connection connection){
		connections.add(connection);
	}

	protected void removeConnection(Connection connection){
		connections.remove(connection);
	}

	/**
	 * Gets a list of the current listeners attached to the server. The returned list should not be modified.
	 * @return A list of the current listeners attached to the server.
	 */
	public List<IServerListener> getListeners(){
		return listeners;
	}

	/**
	 * Attaches the specified listener to the server. If the listener is already attached to this server, it will not be attached again.
	 * @param listener The listener to attach to the server.
	 */
	public void addListener(IServerListener listener){
		if(!listeners.contains(listener))
			listeners.add(listener);
	}

	/**
	 * Detaches the specified listener from the server.
	 * @param listener he listener to detach from the server.
	 */
	public void removeListener(IServerListener listener){
		listeners.remove(listener);
	}

	protected void onConnected(Connection connection){
		for(IServerListener listener : listeners)
			listener.onConnected(connection);
	}

	protected void onDisconnected(Connection connection, DisconnectReason reason){
		for(IServerListener listener : listeners)
			listener.onDisconnected(connection, reason);
		removeConnection(connection);
	}

	protected void onDataReceived(Connection connection, byte[] data){
		for(IServerListener listener : listeners)
			listener.onDataReceived(connection, data);
	}

	/**
	 * Starts the server if it is not running.
	 * @throws IOException Passes any IOException thrown by the {@link ServerSocketFactory#createServerSocket(int)} method.
	 */
	public void start()throws IOException{
		if(running)
			throw new IllegalStateException("Cannot start server when it's already running.");
		else{
			serverSocket = ServerSocketFactory.getDefault().createServerSocket(port);

			thread = new Thread(){
				@Override
				public void run(){
					running = true;
					while(running){
						try{
							Socket clientSocket = serverSocket.accept();
							Connection connection = new Connection(Server.this, clientSocket, connectionCounter++, connectionReadSleepTime);
							addConnection(connection);
							connection.initThread();
							for(IServerListener listener : listeners)
								listener.onConnected(connection);
						}catch(IOException e){
							for(IServerListener listener : listeners)
								listener.onServerException(e);
						}
						try{
							if(sleepTime > 0L)
								Thread.sleep(sleepTime);
						}catch(InterruptedException e){}
					}
				}
			};
			thread.start();
		}
	}

	/**
	 * Stops the server if it is running.
	 * @throws IOException Passes any IOExcepion thrown by {@link ServerSocket#close()}.
	 */
	public void stop()throws IOException{
		if(!running || serverSocket.isClosed())
			throw new IllegalStateException("Cannot stop server when it's already running.");
		else{
			running = false;
			serverSocket.close();
		}
	}
}
